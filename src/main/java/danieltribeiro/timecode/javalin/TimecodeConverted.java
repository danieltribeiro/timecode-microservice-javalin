package danieltribeiro.timecode.javalin;

import danieltribeiro.timecode.Timecode;

class TimecodeConverted {
  private Timecode srcTimecode;
  private Timecode destTimecode;

  /**
   * @return the destTimecode
   */
  public Timecode getDestTimecode() {
    return destTimecode;
  }
  /**
   * @param destTimecode the destTimecode to set
   */
  public TimecodeConverted setDestTimecode(Timecode destTimecode) {
    this.destTimecode = destTimecode;
    return this;
  }

  /**
   * @return the srcTimecode
   */
  public Timecode getSrcTimecode() {
    return srcTimecode;
  }

  /**
   * @param srcTimecode the srcTimecode to set
   */
  public TimecodeConverted setSrcTimecode(Timecode srcTimecode) {
    this.srcTimecode = srcTimecode;
    return this;
  }
}