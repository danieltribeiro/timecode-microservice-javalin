package danieltribeiro.timecode.javalin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import danieltribeiro.timecode.Timecode;

public class ConvertController  {

    public ConvertController(App app) {
        Gson gson = app.getGson();
        ConvertController convertController = this;

        app.getServer().get("/convert/:srcfr/:value/:destfr/:offset", (ctx) -> {
            String s = gson.toJson(convertController.convertSingle(ctx.pathParam("srcfr"), ctx.pathParam("value"), ctx.pathParam("destfr"), ctx.pathParam("offset")));
            ctx.result(s);
        });

        app.getServer().post("/convert/:srcfr/:destfr/:offset", (ctx) -> {
            @SuppressWarnings("unchecked")
            List<Object> list = gson.fromJson(ctx.body(), List.class);
            String s = gson.toJson(convertController.convertMany(ctx.pathParam("srcfr"), ctx.pathParam("destfr"), ctx.pathParam("offset"), list));
            ctx.result(s);
        });

        app.getServer().post("/convert/:srcfr/:destfr/:offset/as-array", (ctx) -> {
            @SuppressWarnings("unchecked")
            List<Object> list = gson.fromJson(ctx.body(), List.class);
            String s = gson.toJson(convertController.convertManyAsArray(ctx.pathParam("srcfr"), ctx.pathParam("destfr"), ctx.pathParam("offset"), list));
            ctx.result(s);
        });


        app.getServer().post("/convert/:srcfr/:destfr/:offset/as-map", (ctx) -> {
            @SuppressWarnings("unchecked")
            List<Object> list = gson.fromJson(ctx.body(), List.class);
            String s = gson.toJson(convertController.convertManyAsMap(ctx.pathParam("srcfr"), ctx.pathParam("destfr"), ctx.pathParam("offset"), list));
            ctx.result(s);
        });
    }

    public Timecode convertSingle(String srcfr, String value, String destfr, String offset) {
        return Timecode.parse(value, srcfr).convertFrameRate(destfr, offset);
    }

    public List<TimecodeConverted> convertManyAsArray(String srcfr, String destfr, String offset, List<Object> list) {
        List<TimecodeConverted> newList = new ArrayList<>();
        for (Object s : list) {
        Timecode tc = Timecode.parse(s.toString(), srcfr);
        newList.add(new TimecodeConverted().setSrcTimecode(tc).setDestTimecode(tc.convertFrameRate(destfr, offset)));
        };
        return newList;
    }


    public List<Timecode> convertMany(String srcfr, String destfr, String offset, List<Object> list) {
        List<Timecode> newList = new ArrayList<>();
        for (Object s : list) {
        newList.add(Timecode.parse(s.toString(), srcfr).convertFrameRate(destfr, offset));
        };
        return newList;
    }


    public Map<String, Timecode> convertManyAsMap(String srcfr, String destfr, String offset, List<Object> list) {
        return list.parallelStream().distinct().collect(Collectors.toMap(s -> s.toString(), s -> Timecode.parse(s.toString(), srcfr).convertFrameRate(destfr, offset)));
    }
}