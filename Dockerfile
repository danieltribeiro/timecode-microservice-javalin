FROM ubuntu:18.04
RUN apt-get update && apt-get install -y gcc zlib1g-dev wget tar
RUN wget https://github.com/oracle/graal/releases/download/vm-1.0.0-rc11/graalvm-ce-1.0.0-rc11-linux-amd64.tar.gz
RUN tar -xzf graalvm-ce-1.0.0-rc11-linux-amd64.tar.gz
ENV PATH /graalvm-ce-1.0.0-rc11/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
WORKDIR /graalvm
COPY . /graalvm
RUN ./gradlew nativeImage

FROM alpine
WORKDIR /graalvm
COPY --from=0 /graalvm/danieltribeiro.timecode.javalin.app .
EXPOSE 8080
CMD ./danieltribeiro.timecode.javalin.app